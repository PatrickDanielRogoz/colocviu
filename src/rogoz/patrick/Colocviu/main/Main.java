package rogoz.patrick.Colocviu.main;

import java.util.Scanner;

public class Main {

    int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        System.out.println("Alegeti un numar dintre cele afisate mai jos");
        System.out.println("---------------------------\n");
        System.out.println("1 - Adauga un produs nou");
        System.out.println("2 - Sterge un produs");
        System.out.println("3 - Editeaza un produs existent");
        System.out.println("4 - Vinde un produs");
        System.out.println("5 - Retrage un produs de la vanzare");
        System.out.println("0 - Iesire");

        selection = input.nextInt();
        return selection;
    }


}